import numpy as np
from scipy.signal import correlate, find_peaks, hilbert
from scipy.ndimage.interpolation import shift
import matplotlib.pyplot as plt
from icecream import ic
from tqdm import tqdm
import copy
import matplotlib.colors as mcolors
from functions import *

colors = mcolors.TABLEAU_COLORS
by_hsv = sorted((tuple(mcolors.rgb_to_hsv(mcolors.to_rgb(color))),
                         name)
                        for name, color in colors.items())
colrs = [name for hsv, name in by_hsv]

import neurolib.utils.functions as func
import neurolib.utils.stimulus as stim


def dom_frequency(ts, dt=0.1, SP_WINDOW=100, peak_h=-20):
    ps = func.getPowerSpectrum(ts,
                              dt,
                              maxfr=100,
                              spectrum_windowsize=SP_WINDOW) # powerspectrum
    peaks, props = find_peaks(ps[1].T, height = peak_h)
    return ps[0][peaks[props['peak_heights'].argmax()]]  # dominant frequency of ts


class ALNRatesStimulus(stim.Stimulus):
    """
    Real ALN Oscillation Period for rate input.
    """

    def __init__(
        self, rates, frperiod, start=None, n=1, seed=None
    ):
        # save parameters as attributes
        self.rates = rates
        self.frperiod = frperiod
        # pass other params to parent class
        super().__init__(
            start=start, end=start + frperiod, n=n, seed=seed
        )

    def generate_input(self, duration, dt):
        # this is a helper function that creates self.times vector
        self._get_times(duration=duration, dt=dt)
        #ic(np.shape(self.times))
        #zeros = [0]*int(self.start/dt)
        #ic(len(zeros))
        #tst = np.hstack((zeros, self.rates))
        how_much = len(self.times) - len(self.rates)
        stim = np.hstack((self.rates, [0]*how_much))
        stim[self.times > self.end] = 0.0

        return self._trim_stim(np.vstack([stim] * self.n))

class ShiftedSinusoidalInput(stim.Stimulus):
    """
    Sinusoidal input with specified phase shift
    """

    def __init__(
        self,
        amplitude,
        frequency,
        peak_at=None,
        phase_shift=None,
        dc_bias=False,
        start=None,
        end=None,
        n=1,
        seed=None,
    ):

        self.amplitude = amplitude
        self.frequency = frequency
        self.peak_at = peak_at
        self.phase_shift = phase_shift or 0
        self.dc_bias = dc_bias
        super().__init__(
            start=start,
            end=end,
            n=n,
            seed=seed,
        )

    def generate_input(self, duration, dt):
        self._get_times(duration=duration, dt=dt)

        period = 1000 / self.frequency

        self.phase_shift = period / 4 - float(self.peak_at)

        sinusoid = self.amplitude * np.sin(2 * np.pi * self.times * (self.frequency / 1000.0) + \
                                           2 * np.pi * self.phase_shift / period)
        if self.dc_bias:
            sinusoid += self.amplitude
        return self._trim_stim(np.vstack([sinusoid] * self.n))

class PRC_ALN_testing:
    '''
    check the applicablity of PRC on the ALN Model (with real input)
    '''
    def __init__(self, model):
        self.model = model

        self.prc = {}
        self.prc_second = {}
        self.prc_mixed = {}
        self.prc_rrate = {}
        self.amp_diffs = {}

        self.frperiod = self.get_freerunning_period()
        self.frperiod_dt = int(self.frperiod / self.model.params.dt)

        self.frcycle = self.get_freerun_cycle()
        print("initialised ...")

    def get_freerunning_period(self, skip_ms=2000):
        '''
        finds peak frequency of oscillation and returns its period in ms
        '''
        self.model.params.duration = 10000 # need 10 s for analyzing period precisely
        self.model.run()

        skip = int(skip_ms / self.model.params.dt)

        domfreq = dom_frequency(self.model.rates_exc[0][skip:], dt=self.model.params.sampling_dt, SP_WINDOW=50000)
        return 1000 / domfreq # return period in ms

    def get_freerun_cycle(self):
        '''
        returns one whole cycle of unperturbed ALN with peak activity in the middle
        '''
        self.model.params.duration = 5000
        self.model.run()

        start = - 3 * self.frperiod_dt
        peaks, _ = find_peaks(self.model.rates_exc[0][start:], prominence=4)
        #ic(peaks[0])

        #ic(self.frperiod_dt)
        return self.model.rates_exc[0][start + peaks[0] + int(0.5*self.frperiod_dt): start + peaks[0] + int(1.5*self.frperiod_dt)]

    def get_freerun_cycle_all(self):
        '''
        returns one whole cycle of unperturbed ALN with peak activity in the middle
        '''
        self.model.params.duration = 5000
        self.model.run()

        start = - 3 * self.frperiod_dt
        peaks, _ = find_peaks(self.model.rates_exc[0][start:], prominence=4)

        startt = start + peaks[0] + int(0.5 * self.frperiod_dt)
        endt = start + peaks[0] + int(1.5 * self.frperiod_dt)

        cycles = {}
        for var in ['rates_exc',
                    'rates_inh',
                    'mufe',
                    'mufi',
                    'sigmae_arr',
                    'sigmai_arr',
                    'seem',
                    'seim',
                    'siem',
                    'siim',
                    'seev',
                    'seiv',
                    'siev',
                    'siiv',]:
                    #'tau_exc_a',
                    #'tau_inh_a']:
            cycles[var] = self.model[var][0][startt : endt]

        return cycles



    def test_rapid_decay(self,
                 itype="current",
                 stim_type="step",
                 amplitude=0.1,
                 stim_len=1,
                 Ke_gl=10,
                 num_points=20):

        '''
        Test the applicability of PRC as in "rapid decay" by comparing
        amplitude of rate at the time point one cycle after stimulus
        '''
        # delete data of possible previous runs
        self.amp_diffs = {}

        # run model to store state vector of a point inside LC
        self.model.params.duration = 6000
        self.model.run(continue_run=True)

        # create reference deepcopy without stimulation
        aln_ref = copy.deepcopy(self.model)
        aln_ref.params.duration = 10000
        aln_ref.run()

        # find peaks in reference run to determine stimulus phase
        peaks, props = find_peaks(aln_ref.rates_exc[0].T, height = -20)
        f_peak_ref = peaks[0] * aln_ref.params.dt
        s_peak_ref = peaks[1] * aln_ref.params.dt
        t_peak_ref = peaks[2] * aln_ref.params.dt
        #ic(f_peak_ref, s_peak_ref, t_peak_ref)

        # create array of input_delays s.t. evenly spaced points on LC are hit with input
        input_delays = np.linspace(0, self.frperiod, num_points+1)
        input_delays = input_delays[:-1]
        #ic(input_delays)

        for i, delay in enumerate(input_delays):

            aln = copy.deepcopy(self.model)
            aln.params.duration = 10000
            aln.params.Ke_gl = Ke_gl # regardless of current/rate input

            if itype in ["current", "rate"]:
                if stim_type=="sine":
                    stimulus = ShiftedSinusoidalInput(
                        amplitude=amplitude,
                        frequency=1000 / self.frperiod,
                        peak_at=delay,
                        start=0,
                        end=self.frperiod,
                    ).as_array(aln.params.duration, aln.params.dt)

                    #delay = 0 #because of the plotting
                else:
                    stimulus = stim.StepInput(
                        step_size = amplitude,
                        start = delay,
                        end = delay+stim_len,
                        n = 1
                    ).as_array(aln.params.duration, aln.params.dt)

                if itype == "current":
                    aln.params["ext_exc_current"] = stimulus
                elif itype == "rate":
                    aln.params["ext_exc_rate"] = stimulus * 1e-3 # convert Hz to kHz

            elif itype == "real":

                stimulus = ALNRatesStimulus(
                    start = delay,
                    #start = int(0.5*self.frperiod) + delay,
                    rates = self.frcycle,
                    frperiod = self.frperiod
                ).as_array(aln.params.duration, aln.params.dt)

                aln.params["ext_exc_rate"] = stimulus * 1e-3 # convert Hz to kHz
            else:
                print("input must be 'current', 'rate', or 'real'")

            aln.run(continue_run=True)

            # determine phase of stimulus input
            if itype == "real":
                stim_len = self.frperiod

            input_peak = delay + stim_len / 2

            if input_peak < f_peak_ref:
                phase_t = self.frperiod - (f_peak_ref - input_peak) # period - time to period end
            elif input_peak < s_peak_ref:
                phase_t = self.frperiod - (s_peak_ref - input_peak)
            elif input_peak < t_peak_ref:
                phase_t = self.frperiod - (t_peak_ref - input_peak)
            else:
                print("ERROR: stimulus delay > freerunning period")
                ic(delay, self.frperiod)

            # find peaks in stimulated runs and compare to freerunning reference
            peaks, props = find_peaks(aln.rates_exc[0].T, prominence=3)
            f_peak = peaks[0] * aln.params.dt  # in ms
            s_peak = peaks[1] * aln.params.dt
            t_peak = peaks[2] * aln.params.dt

            # find phase shift to reference run by crosscorrelation
            # phase_shift in [-1, 1]

            p_shift, dt_shift = phase_shift(aln_ref.rates_exc[0],
                                      aln.rates_exc[0],
                                      duration=6000,
                                      dt=aln.params.dt)
            #ic(p_shift)
            # evaluation point:
            eval_point = int((self.frperiod + delay) / aln.params.dt)
            #ic(self.frperiod + delay)
            #ic(eval_point)
            # calculate amplitude difference of stimulated and shifted reference run
            ref_shifted = shift(aln_ref.rates_exc[0], int(dt_shift), cval=0)

            amp_diff = aln.rates_exc[0][eval_point] - ref_shifted[eval_point]
            self.amp_diffs[phase_t] = amp_diff

        self.amp_diffs = dict(sorted(self.amp_diffs.items()))


    def test_rapid_decay_L2_norm_all(self,
                 itype="current",
                 stim_type="step",
                 amplitude=0.1,
                 stim_len=1,
                 Ke_gl=10,
                 num_points=20):

        '''
        Test the applicability of PRC as in "rapid decay" by comparing the
        value of all dynamical variables with the unperturbed trajectory one
        cycle after stimulus onset. Computes normalized deviation of all variables.
        '''
        # delete data of possible previous runs
        self.amp_diffs = {}

        # run model to store state vector of a point inside LC
        self.model.params.duration = 6000
        self.model.run(continue_run=True)

        # create reference deepcopy without stimulation
        aln_ref = copy.deepcopy(self.model)
        aln_ref.params.duration = 10000
        aln_ref.run()

        # find peaks in reference run to determine stimulus phase
        peaks, props = find_peaks(aln_ref.rates_exc[0].T, height = -20)
        f_peak_ref = peaks[0] * aln_ref.params.dt
        s_peak_ref = peaks[1] * aln_ref.params.dt
        t_peak_ref = peaks[2] * aln_ref.params.dt
        #ic(f_peak_ref, s_peak_ref, t_peak_ref)

        # create array of input_delays s.t. evenly spaced points on LC are hit with input
        input_delays = np.linspace(0, self.frperiod, num_points+1)
        input_delays = input_delays[:-1]
        #ic(input_delays)

        for i, delay in tqdm(enumerate(input_delays), total=len(input_delays)):

            aln = copy.deepcopy(self.model)
            aln.params.duration = 10000
            aln.params.Ke_gl = Ke_gl # regardless of current/rate input

            if itype in ["current", "rate"]:
                if stim_type=="sine":
                    stimulus = ShiftedSinusoidalInput(
                        amplitude=amplitude,
                        frequency=1000 / self.frperiod,
                        peak_at=delay,
                        start=0,
                        end=self.frperiod,
                    ).as_array(aln.params.duration, aln.params.dt)

                    #delay = 0 #because of the plotting
                else:
                    stimulus = stim.StepInput(
                        step_size = amplitude,
                        start = delay,
                        end = delay+stim_len,
                        n = 1
                    ).as_array(aln.params.duration, aln.params.dt)

                if itype == "current":
                    aln.params["ext_exc_current"] = stimulus
                elif itype == "rate":
                    aln.params["ext_exc_rate"] = stimulus * 1e-3 # convert Hz to kHz

            elif itype == "real":

                stimulus = ALNRatesStimulus(
                    start = delay,
                    #start = int(0.5*self.frperiod) + delay,
                    rates = self.frcycle,
                    frperiod = self.frperiod
                ).as_array(aln.params.duration, aln.params.dt)

                aln.params["ext_exc_rate"] = stimulus * 1e-3 # convert Hz to kHz
            else:
                print("input must be 'current', 'rate', or 'real'")

            aln.run(continue_run=True)

            # determine phase of stimulus input
            if itype == "real":
                stim_len = self.frperiod

            input_peak = delay + stim_len / 2

            if input_peak < f_peak_ref:
                phase_t = self.frperiod - (f_peak_ref - input_peak) # period - time to period end
            elif input_peak < s_peak_ref:
                phase_t = self.frperiod - (s_peak_ref - input_peak)
            elif input_peak < t_peak_ref:
                phase_t = self.frperiod - (t_peak_ref - input_peak)
            else:
                print("ERROR: stimulus delay > freerunning period")
                ic(delay, self.frperiod)

            # find peaks in stimulated runs and compare to freerunning reference
            peaks, props = find_peaks(aln.rates_exc[0].T, prominence=3)
            f_peak = peaks[0] * aln.params.dt  # in ms
            s_peak = peaks[1] * aln.params.dt
            t_peak = peaks[2] * aln.params.dt

            # find phase shift to reference run by crosscorrelation
            # phase_shift in [-1, 1]

            p_shift, dt_shift = phase_shift(aln_ref.rates_exc[0],
                                      aln.rates_exc[0],
                                      duration=6000,
                                      dt=aln.params.dt)
            #ic(p_shift)
            # evaluation point:
            eval_point = int((self.frperiod + delay) / aln.params.dt)
            #ic(self.frperiod + delay)
            #ic(eval_point)
            # calculate amplitude difference of stimulated and shifted reference run
            ref_shifted = shift(aln_ref.rates_exc[0], int(dt_shift), cval=0)

            assert 'z1ee_ar' in self.model.output_vars, "Need SaveSteps model to calc distance from LC with all variables"
            # add relative difference of all state variables
            #
            # In order to calc rel. difference, we need to find the amplitude excursion of one cycle

            cycles = self.get_freerun_cycle_all()

            # rates_exc, rates_inh (firing rate of E and I)
            # mufe, mufi (mean filtered input of E and I)
            # sigma_e, sigma_i (std of filtered input of E and I)
            # seem, seim (mean synaptic input to E)
            # siem, siim (mean synaptic input to I)
            # seev, seiv (std of syn. input to E)
            # siev, siiv (std of syn. input to I)
            amp_diff = 0
            rel_diffs = []
            for var in ['rates_exc',
                        'rates_inh',
                        'mufe',
                        'mufi',
                        'sigmae_arr',
                        'sigmai_arr',
                        'seem',
                        'seim',
                        'siem',
                        'siim',
                        'seev',
                        'seiv',
                        'siev',
                        'siiv']:
                # 1. calc max and min of variable LC
                amp = np.max(cycles[var]) - np.min(cycles[var])
                # 2. get corresponding point on LC
                var_shifted = shift(aln_ref[var][0], int(dt_shift), cval=0)
                # 3. relativ diff = |simulated_point - LC_point| / amp
                rel_diff = np.abs( (aln[var][0][eval_point] - var_shifted[eval_point]) / amp )
                rel_diffs.append(rel_diff)
                amp_diff += rel_diff

            amp_diff /= 14 # 14 variables diffs added up
            self.amp_diffs[phase_t] = rel_diffs

        self.amp_diffs = dict(sorted(self.amp_diffs.items()))



    def plot_test_prc(self,
                 itype="current",
                 stim_type="step",
                 amplitude=0.1,
                 stim_len=1,
                 Ke_gl=10,
                 num_points=20):
        '''
        Test the applicability of PRC as in "rapid decay"
        '''
        # run model to store state vector of a point inside LC
        self.model.params.duration = 6000
        self.model.run(continue_run=True)

        # create reference deepcopy without stimulation
        aln_ref = copy.deepcopy(self.model)
        aln_ref.params.duration = 10000
        aln_ref.run()

        # find peaks in reference run to determine stimulus phase
        peaks, props = find_peaks(aln_ref.rates_exc[0].T, height = -20)
        f_peak_ref = peaks[0] * aln_ref.params.dt
        s_peak_ref = peaks[1] * aln_ref.params.dt
        t_peak_ref = peaks[2] * aln_ref.params.dt
        #ic(f_peak_ref, s_peak_ref, t_peak_ref)

        # create array of input_delays s.t. evenly spaced points on LC are hit with input
        input_delays = np.linspace(0, self.frperiod, num_points+1)
        input_delays = input_delays[:-1]
        #ic(input_delays)

        for i, delay in enumerate(input_delays):

            aln = copy.deepcopy(self.model)
            aln.params.duration = 10000
            aln.params.Ke_gl = Ke_gl # regardless of current/rate input

            if itype in ["current", "rate"]:
                if stim_type=="sine":
                    stimulus = ShiftedSinusoidalInput(
                        amplitude=amplitude,
                        frequency=1000 / self.frperiod,
                        peak_at=delay,
                        start=0,
                        end=self.frperiod,
                    ).as_array(aln.params.duration, aln.params.dt)

                    delay = 0 #because of the plotting
                else:
                    stimulus = stim.StepInput(
                        step_size = amplitude,
                        start = delay,
                        end = delay+stim_len,
                        n = 1
                    ).as_array(aln.params.duration, aln.params.dt)

                if itype == "current":
                    aln.params["ext_exc_current"] = stimulus
                elif itype == "rate":
                    aln.params["ext_exc_rate"] = stimulus * 1e-3 # convert Hz to kHz

            elif itype == "real":

                stimulus = ALNRatesStimulus(
                    start = delay,
                    #start = int(0.5*self.frperiod) + delay,
                    rates = self.frcycle,
                    frperiod = self.frperiod
                ).as_array(aln.params.duration, aln.params.dt)

                aln.params["ext_exc_rate"] = stimulus * 1e-3 # convert Hz to kHz
            else:
                print("input must be 'current', 'rate', or 'real'")

            aln.run(continue_run=True)

            # determine phase of stimulus input
            if itype == "real":
                stim_len = self.frperiod

            input_peak = delay + stim_len / 2

            if input_peak < f_peak_ref:
                phase_t = self.frperiod - (f_peak_ref - input_peak) # period - time to period end
            elif input_peak < s_peak_ref:
                phase_t = self.frperiod - (s_peak_ref - input_peak)
            elif input_peak < t_peak_ref:
                phase_t = self.frperiod - (t_peak_ref - input_peak)
            else:
                print("ERROR: stimulus delay > freerunning period")
                ic(delay, self.frperiod)

            # find peaks in stimulated runs and compare to freerunning reference
            peaks, props = find_peaks(aln.rates_exc[0].T, prominence=3)
            f_peak = peaks[0] * aln.params.dt  # in ms
            s_peak = peaks[1] * aln.params.dt
            t_peak = peaks[2] * aln.params.dt

            # find phase shift to reference run by crosscorrelation
            # phase_shift in [-1, 1]

            p_shift, dt_shift = phase_shift(aln_ref.rates_exc[0],
                                      aln.rates_exc[0],
                                      duration=6000,
                                      dt=aln.params.dt)
            #ic(p_shift)
            # evaluation point:
            eval_point = int((self.frperiod + delay) / aln.params.dt)
            #ic(self.frperiod + delay)
            #ic(eval_point)
            # calculate amplitude difference of stimulated and shifted reference run
            ref_shifted = shift(aln_ref.rates_exc[0], int(dt_shift), cval=0)

            amp_diff = aln.rates_exc[0][eval_point] - ref_shifted[eval_point]
            #ic(aln.rates_exc[0][eval_point], ref_shifted[eval_point])
            #ic(ref_shifted[eval_point])

            fig, axs = plt.subplots(2,1, figsize=(6, 2), dpi=150)
            axs[0].set_title("amplitude diff: {0:.3f} Hz".format(amp_diff))
            axs[0].plot(aln.t[0:int(100/aln.params.dt)],
                        aln.rates_exc[0].T[0:int(100/aln.params.dt)], lw=4, c="tab:orange", label="stimulated")
            axs[0].plot(aln.t[0:int(100/aln.params.dt)],
                        aln_ref.rates_exc[0].T[0:int(100/aln.params.dt)], c="tab:blue", label="reference")
            axs[0].plot(aln.t[0:int(100/aln.params.dt)],
                        ref_shifted.T[0:int(100/aln.params.dt)], "--", color="black", label="shifted ref")
            axs[0].axvline(delay, color="grey")
            #axs[0].axvspan(delay, delay+stim_len, facecolor='grey', alpha=0.4, label="stimulus")
            axs[0].axvline(self.frperiod + delay, color="grey")
            axs[0].axvspan(self.frperiod + delay, self.frperiod + delay+stim_len, facecolor='grey', alpha=0.4)
            #ic(len(aln.t), len(stimulus))
            axs[0].plot(aln.t[0:int(100/aln.params.dt)],
                        stimulus[0][0:int(100/aln.params.dt)]*100, c="grey")

            axs[1].plot(aln.t[-int(100/aln.params.dt):],
                        aln.rates_exc[0].T[-int(100/aln.params.dt):], lw=4, c="tab:orange", label="stimulated")
            axs[1].plot(aln.t[-int(100/aln.params.dt):],
                        aln_ref.rates_exc[0].T[-int(100/aln.params.dt):], c="tab:blue", label="reference")
            axs[1].plot(aln.t[-int(100/aln.params.dt):],
                        ref_shifted.T[-int(100/aln.params.dt):], "--", color="black", label="shifted ref")
            #ax.set_xlim(1500, 2500)
            axs[0].legend()
            axs[1].set_xlabel("t [ms]")
            #self.amp_diffs[delay] = amp_diff
            del(aln)


class PRC_ALN:
    '''
    calcuate PRC and related measures of given ALN Model
    '''
    def __init__(self, model):
        self.model = model

        self.prc = {}
        self.prc_second = {}
        self.prc_mixed = {}
        self.prc_rrate = {}
        self.prc_xcorr = {}

        self.frperiod = self.get_freerunning_period()
        self.frperiod_dt = int(self.frperiod / self.model.params.dt)
        self.frperiod_sdt = int(self.frperiod / self.model.params.sampling_dt)

        self.frcycle = self.get_freerun_cycle()
        print("initialised ...")

    def get_freerunning_period(self):
        '''
        finds peak frequency of oscillation and returns its period in ms
        '''
        self.model.params.duration = 100000 # need 100 s for analyzing period precisely
        self.model.run()

        skip_ms = 5000
        skip = int(skip_ms / self.model.params.sampling_dt)

        domfreq = dom_frequency(self.model.rates_exc[0][skip:],
                                dt=self.model.params.sampling_dt,
                                SP_WINDOW=20000)
        ic(domfreq)

        return 1000 / domfreq # return period in ms

    def get_freerun_cycle(self):
        '''
        returns one whole cycle of unperturbed ALN with peak activity in the middle
        '''
        self.model.params.duration = 5000
        self.model.run()

        start = - 3 * self.frperiod_sdt
        peaks, _ = find_peaks(self.model.rates_exc[0][start:], prominence=4)
        #ic(peaks[0])

        #ic(self.frperiod_dt)
        return self.model.rates_exc[0][start + peaks[0] + int(0.5*self.frperiod_sdt): start + peaks[0] + int(1.5*self.frperiod_sdt)]

    def plot_prc(self,
                 itype="current",
                 stim_type="step",
                 amplitude=0.1,
                 stim_len=1,
                 num_points=20
                 ):
        '''
        Show timeseries of stimulated system to verify phase advances/delays
        '''
        # run model to store state vector of a point inside LC
        self.model.params.duration = 10000
        self.model.run(continue_run=True)

        # create reference deepcopy without stimulation
        aln_ref = copy.deepcopy(self.model)
        aln_ref.params.duration = 2. * self.frperiod
        aln_ref.run()
        # find peaks in reference run to determine stimulus phase
        peaks, props = find_peaks(aln_ref.rates_exc[0].T, height = -20)
        ic(peaks)
        f_peak_ref = peaks[0] * aln_ref.params.dt

        fig, axs = plt.subplots(num_points, 1, figsize=(12, 2*num_points), dpi=150, sharex=True)

        # create array of input_delays s.t. evenly spaced points on LC are hit with input
        input_delays = np.linspace(0, self.frperiod, num_points+1)
        input_delays = input_delays[:-1]
        ic(input_delays)
        for i, delay in enumerate(input_delays):

            aln = copy.deepcopy(self.model)
            aln.params.duration = 2. * self.frperiod

            if stim_type=="sine":
                stimulus = ShiftedSinusoidalInput(
                    amplitude=amplitude,
                    frequency=1000 / self.frperiod,
                    peak_at=delay,
                    start=0,
                    end=self.frperiod,
                ).as_array(aln.params.duration, aln.params.dt)

                #delay = 0 #because of the plotting
            else:
                stimulus = stim.StepInput(
                    step_size = amplitude,
                    start = delay,
                    end = delay+stim_len,
                    n = 1
                ).as_array(aln.params.duration, aln.params.dt)

            assert itype in ["current", "rate"], "input kind must be 'current' or 'rate'!"

            if itype == "current":
                aln.params["ext_exc_current"] = stimulus
            elif itype == "rate":
                aln.params["ext_exc_rate"] = stimulus * 1e-3 # convert kHz to Hz

            aln.run(continue_run=True)
            #ic(len(stimulus))
            #ic(max(aln.params['ext_exc_current']))


            axs[i].plot(aln.t, aln.rates_exc[0].T, color="tab:red", lw=3, label="stim. run")
            axs[i].plot(aln_ref.t, aln_ref.rates_exc[0].T, "--",color="black", lw=1, label="free run")


            if 'z1ee_a' in self.model.output_vars: # if savesteps model, also print synaptic variable
                axs[i].plot(aln.t, aln.seem[0].T * 20, "-.", color="tab:blue", lw=0.5, label="seem")
                axs[i].axhline(y=20)
                axs[i].plot(aln.t, aln.z1ee_a[0].T * 3, "-.", color="tab:red", lw=0.5, label="z1ee")
            else:
                pass
            #axs[i].scatter(fpeak, aln.rates_exc[0][fpeak])
            #axs[i].scatter(peaks[0]*aln.params.dt + self.frperiod, fpeak)
            axs[i].plot(aln.t, stimulus[0]*100, color=colrs[i%10])
        axs[-1].set_xlabel("t [ms]")
        axs[0].legend()
        axs[0].set_title("{0:.2f} mV/ms x {1:.2f} ms".format(amplitude, stim_len), y=0.85)
        fig.tight_layout()


    def plot_prc_0(self, itype="current", amplitude=0.1, stim_len=1, num_points=20):
        '''
        Show timeseries of stimulated system to verify phase advances/delays
        '''
        # run model to store state vector of a point inside LC
        self.model.params.duration = 10000
        self.model.run(continue_run=True)

        # create reference deepcopy without stimulation
        aln_ref = copy.deepcopy(self.model)
        aln_ref.params.duration = 2. * self.frperiod
        aln_ref.run()
        # find peaks in reference run to determine stimulus phase
        peaks, props = find_peaks(aln_ref.rates_exc[0].T, height = -20)
        f_peak_ref = peaks[0] * aln_ref.params.dt



        # create array of input_delays s.t. evenly spaced points on LC are hit with input
        input_delays = np.linspace(0, self.frperiod, num_points)

        # calc num of plots to make
        num_plots = np.count_nonzero(np.isclose(input_delays, f_peak_ref, atol=1))
        fig, axs = plt.subplots(num_plots, 1, figsize=(5, 5*num_plots), dpi=200, sharex=True)

        j = 0
        for i, delay in enumerate(input_delays):
            if not np.isclose(delay, f_peak_ref, atol=1): # only stimuli close to peak
                continue

            aln = copy.deepcopy(self.model)
            aln.params.duration = 2. * self.frperiod
            stimulus = stim.StepInput(
                step_size = amplitude,
                start = delay,
                end = delay+stim_len,
                n = 1
            ).as_array(aln.params.duration, aln.params.dt)

            assert itype in ["current", "rate"], "input kind must be 'current' or 'rate'!"

            if itype == "current":
                aln.params["ext_exc_current"] = stimulus
            elif itype == "rate":
                aln.params["ext_exc_rate"] = stimulus

            aln.run(continue_run=True)
            #ic(len(stimulus))
            #ic(max(aln.params['ext_exc_current']))

            peaks, props = find_peaks(aln.rates_exc[0].T, height = 50, prominence=4)
            f_peak = peaks[0] * aln.params.dt  # in ms

            start = f_peak_ref - 1
            stop = f_peak_ref + 1
            axs[j].plot(aln.t, aln.rates_exc[0].T, color=colrs[i%10], lw=2, label="stim. run")
            axs[j].plot(aln_ref.t, aln_ref.rates_exc[0].T, "--",color="black", lw=1, label="free run")

            axs[j].axvline(f_peak_ref, color="black", lw=0.9)
            axs[j].axvline(f_peak, color=colrs[i%10], lw=0.9)
            #axs[i].scatter(peaks[0]*aln.params.dt + self.frperiod, fpeak)
            axs[j].plot(aln.t, stimulus[0]*10 + 50, color=colrs[i%10], alpha=0.8, lw=0.8)
            axs[j].set_xlim(start, stop)
            axs[j].set_ylim(45, 55)
            j += 1

        axs[-1].set_xlabel("t [ms]")
        axs[0].legend()


    def calc_prc(self,
                 itype="current",
                 stim_type="step",
                 amplitude=0.1,
                 stim_len=1,
                 num_points=20):
        '''
        Calculate PRC values (T_fr - T_s) by comparing next peak of unperturbed and perturbed
        trajectories. Very fast.
        '''

        # run model to store state vector of a point inside LC
        self.model.params.duration = 10000
        self.model.run(continue_run=True)

        # create reference deepcopy without stimulation
        aln_ref = copy.deepcopy(self.model)
        aln_ref.params.duration = 5. * self.frperiod
        aln_ref.run()
        # find peaks in reference run to determine stimulus phase
        peaks, props = find_peaks(aln_ref.rates_exc[0].T, height = -20)
        f_peak_ref = peaks[0] * aln_ref.params.dt
        s_peak_ref = peaks[1] * aln_ref.params.dt
        ic(f_peak_ref, s_peak_ref)
        # create array of input_delays s.t. evenly spaced points on LC are hit with input
        input_delays = np.linspace(0, self.frperiod, num_points)
        #ic(input_delays)
        for i, delay in enumerate(input_delays):

            aln = copy.deepcopy(self.model)
            aln.params.duration = 5. * self.frperiod

            if stim_type=="sine":
                stimulus = ShiftedSinusoidalInput(
                    amplitude=amplitude,
                    frequency=1000 / self.frperiod,
                    peak_at=delay,
                    start=0,
                    end=self.frperiod,
                ).as_array(aln.params.duration, aln.params.dt)

                #delay = 0 #because of the plotting
            else:
                stimulus = stim.StepInput(
                    step_size = amplitude,
                    start = delay,
                    end = delay+stim_len,
                    n = 1
                ).as_array(aln.params.duration, aln.params.dt)

            assert itype in ["current", "rate"], "input kind must be 'current' or 'rate'!"

            if itype == "current":
                aln.params["ext_exc_current"] = stimulus
            elif itype == "rate":
                aln.params["ext_exc_rate"] = stimulus

            aln.run(continue_run=True)

            # determine phase of stimulus input
            if delay < f_peak_ref:
                phase_t = self.frperiod - (f_peak_ref - delay) # period - time to period end
            elif delay < s_peak_ref:
                phase_t = self.frperiod - (s_peak_ref - delay)
            else:
                print("ERROR: stimulus delay > freerunning period")
                ic(delay, self.frperiod)

            # find peaks in stimulated runs and compare to freerunning reference
            peaks, props = find_peaks(aln.rates_exc[0].T, height = 50, prominence=4)
            f_peak = peaks[0] * aln.params.dt  # in ms
            s_peak = peaks[1] * aln.params.dt

            if delay < f_peak: # stimulus arrives before first peak
                phase_response = f_peak_ref - f_peak # PRC = T_fr - Ts, so difference in peak of activity
            elif delay < s_peak:
                phase_response = s_peak_ref - s_peak
            else:
                print("ERROR: stimulus delay too large")

            if phase_response > 1:
                ic(delay)
                ic(f_peak_ref, f_peak)
                ic(s_peak_ref, s_peak)
            self.prc[phase_t] = phase_response

        # sort dict by phase
        self.prc = dict(sorted(self.prc.items()))
        return self.prc



    def plot_prc_realistic(self,
                           Ke_gl=100,
                           num_points=20,
                           plot_all=False):
        '''
        Show timeseries of stimulated system to verify phase advances/delays
        '''
        # run model to store state vector of a point inside LC
        self.model.params.duration = 10000
        self.model.run(continue_run=True)

        # create reference deepcopy without stimulation
        aln_ref = copy.deepcopy(self.model)
        aln_ref.params.duration = 3. * self.frperiod
        aln_ref.run()
        # find peaks in reference run to determine stimulus phase
        peaks, props = find_peaks(aln_ref.rates_exc[0].T, prominence=3)
        ic(peaks)
        f_peak_ref = peaks[0] * aln_ref.params.dt
        s_peak_ref = peaks[1] * aln_ref.params.dt
        t_peak_ref = peaks[2] * aln_ref.params.dt

        fig, axs = plt.subplots(num_points, 1, figsize=(5, 3*num_points), dpi=200, sharex=True)

        # create array of input_delays s.t. evenly spaced points on LC are hit with input
        input_delays = np.linspace(0, self.frperiod, num_points)
        #ic(input_delays)
        for i, delay in enumerate(input_delays):
            aln = copy.deepcopy(self.model)
            aln.params.duration = 3. * self.frperiod
            aln.params.Ke_gl = Ke_gl

            stimulus = ALNRatesStimulus(
                start = delay,
                #start = int(0.5*self.frperiod) + delay,
                rates = self.frcycle,
                frperiod = self.frperiod
            ).as_array(aln.params.duration, aln.params.dt)

            aln.params["ext_exc_rate"] = stimulus * 1e-3 # convert Hz to kHz

            aln.run(continue_run=True)


            axs[i].plot(aln.t, aln.rates_exc[0].T, color=colrs[(i+2)%10], lw=2, label="stim. run")
            axs[i].plot(aln_ref.t, aln_ref.rates_exc[0].T, "--",color="black", lw=1, label="free run")
            #axs[i].plot(aln.t, stimulus[0] / 2, linestyle=(0, (1, 1)), color="black", alpha=0.6, label="stimulus")
            axs[i].plot(aln.t, stimulus[0] / 2, color="black", alpha=0.6, label="stimulus")
            if ('z1ee_a' in self.model.output_vars) and (plot_all==True): # if savesteps model, also print synaptic variable
                axs[i].plot(aln.t, aln.seem[0].T * 15, "-.", color="tab:blue", lw=0.5, label="seem")
                axs[i].axhline(y=15)
                axs[i].plot(aln.t, aln.z1ee_a[0].T * 3, "-.", color="tab:red", lw=0.5, label="z1ee")
            #axs[i].scatter(fpeak, aln.rates_exc[0][fpeak])
            #axs[i].scatter(peaks[0]*aln.params.dt + self.frperiod, fpeak)


            # determine phase of stimulus input
            stim_len = self.frperiod / 2
            input_peak = delay + stim_len
            if input_peak < f_peak_ref:
                phase_t = self.frperiod - (f_peak_ref - input_peak) # period - time to period end
            elif input_peak < s_peak_ref:
                phase_t = self.frperiod - (s_peak_ref - input_peak)
            elif input_peak < t_peak_ref:
                phase_t = self.frperiod - (t_peak_ref - input_peak)
            else:
                print("ERROR: stimulus delay > freerunning period")
                ic(delay, self.frperiod)

             # find peaks in stimulated runs and compare to freerunning reference
            peaks, props = find_peaks(aln.rates_exc[0].T, prominence=3)
            f_peak = peaks[0] * aln.params.dt  # in ms
            s_peak = peaks[1] * aln.params.dt
            t_peak = peaks[2] * aln.params.dt

            if input_peak < f_peak - self.frperiod/4 : # stimulus arrives long (1/4 period) before first peak
                phase_response = f_peak_ref - f_peak # PRC = T_fr - Ts, so difference in peak of activity
            elif input_peak < s_peak - self.frperiod/4: # long before second peak
                phase_response = s_peak_ref - s_peak
            else:
                phase_response = t_peak_ref - t_peak

            axs[i].set_title("phase:{0:.2f}, response: {1:.2f}".format(phase_t, phase_response))
        axs[-1].set_xlabel("t [ms]")
        axs[0].legend()
        #axs[0].set_title("{0:.2f} mV/ms x {1:.2f} ms".format(amplitude, stim_len), y=0.85)
        fig.tight_layout()



    def calc_prc_xcorr(self,
                       itype="current",
                       stim_type="step",
                       amplitude=0.1,
                       stim_len=1,
                       Ke_gl=10,
                       num_points=20
        ):
        '''
        Calculate PRC values (T_fr - T_s) by crosscorrelation of unperturbed
        and perturbed timeseries. Slower since long simulations needed, but non
        plus ultra. 
        '''
        # delete previous entries
        self.prc_xcorr = {}
        # run model to store state vector of a point inside LC
        self.model.params.duration = 5000
        self.model.run(continue_run=True)

        # create reference deepcopy without stimulation
        aln_ref = copy.deepcopy(self.model)
        aln_ref.params.duration = 30000
        aln_ref.run()

        # find peaks in reference run to determine stimulus phase
        peaks, props = find_peaks(aln_ref.rates_exc[0].T, height = -20)
        f_peak_ref = peaks[0] * aln_ref.params.sampling_dt
        s_peak_ref = peaks[1] * aln_ref.params.sampling_dt
        t_peak_ref = peaks[2] * aln_ref.params.sampling_dt

        # create array of input_delays s.t. evenly spaced points on LC are hit with input
        input_delays = np.linspace(0, self.frperiod, num_points + 1)
        input_delays = input_delays[:-1]
        #ic(input_delays)
        for i, delay in tqdm(enumerate(input_delays), total=len(input_delays)):

            aln = copy.deepcopy(self.model)
            aln.params.duration = 30000
            aln.params.Ke_gl = Ke_gl

            if itype in ["current", "rate"]:
                if stim_type=="sine":
                    stimulus = ShiftedSinusoidalInput(
                        amplitude=amplitude,
                        frequency=1000 / self.frperiod,
                        peak_at=delay,
                        start=0,
                        end=self.frperiod,
                    ).as_array(aln.params.duration, aln.params.dt)

                    #delay = 0 #because of the plotting
                else:
                    stimulus = stim.StepInput(
                        step_size = amplitude,
                        start = delay,
                        end = delay+stim_len,
                        n = 1
                    ).as_array(aln.params.duration, aln.params.dt)

                if itype == "current":
                    aln.params["ext_exc_current"] = stimulus
                elif itype == "rate":
                    aln.params["ext_exc_rate"] = stimulus

            elif itype == "real":
                stimulus = ALNRatesStimulus(
                    start = delay,
                    rates = self.frcycle,
                    frperiod = self.frperiod
                ).as_array(aln.params.duration, aln.params.dt)

                aln.params["ext_exc_rate"] = stimulus * 1e-3 # convert Hz to kHz

            else:
                print("input must be 'current', 'rate', or 'real'")
                return

            aln.run(continue_run=True)

            # determine phase of stimulus input
            if itype == "real":
                input_peak = delay + self.frperiod / 2
            elif stim_type == "sine":
                input_peak = delay # sine peak exactly at delay
            else:
                input_peak = delay + stim_len / 2


            if input_peak < f_peak_ref:
                phase_t = self.frperiod - (f_peak_ref - input_peak) # period - time to period end
            elif input_peak < s_peak_ref:
                phase_t = self.frperiod - (s_peak_ref - input_peak)
            elif input_peak < t_peak_ref:
                phase_t = self.frperiod - (t_peak_ref - input_peak)
            else:
                print("ERROR: stimulus delay > freerunning period")
                ic(delay, self.frperiod)

            # reduce phase to period interval
            if phase_t > self.frperiod:
                phase_t -= self.frperiod

            # find phase shift to reference run by crosscorrelation
            # we must have phase_shift(shifted, original) to be consistent with PRC definition
            # PRC = T_free - T_shifted
            # delivers phase_shift in [-1, 1] and dt_shift
            p_shift, dt_shift = phase_shift(aln.rates_exc[0],
                                      aln_ref.rates_exc[0],
                                      duration=15000,
                                      dt=aln.params.sampling_dt)


            phase_response = dt_shift * aln.params.sampling_dt # time shift in ms
            self.prc_xcorr[phase_t] = phase_response

        # sort dict by phase
        self.prc_xcorr = dict(sorted(self.prc_xcorr.items()))
