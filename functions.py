import numpy as np
from scipy.signal import correlate, find_peaks, hilbert
import matplotlib.pyplot as plt
from icecream import ic
import copy
import neurolib.utils.functions as func  
import neurolib.utils.stimulus as stim




# from neurolib.utils.functions
def construct_stimulus(
    stim="dc",
    duration=6000,
    dt=0.1,
    stim_amp=0.2,
    stim_freq=1,
    stim_bias=0,
    n_periods=None,
    nostim_before=0,
    nostim_after=0,
):
    """Constructs a stimulus that can be applied to a model

    :param stim: Stimulation type: 'ac':oscillatory stimulus, 'dc': stimple step current, 
                'rect': step current in negative then positive direction with slowly
                decaying amplitude, used for bistability detection, defaults to 'dc'
    :type stim: str, optional
    :param duration: Duration of stimulus in ms, defaults to 6000
    :type duration: int, optional
    :param dt: Integration time step in ms, defaults to 0.1
    :type dt: float, optional
    :param stim_amp: Amplitude of stimulus (for AdEx: in mV/ms, multiply by conductance C to get current in pA), defaults to 0.2
    :type stim_amp: float, optional
    :param stim_freq: Stimulation frequency, defaults to 1
    :type stim_freq: int, optional
    :param stim_bias: Stimulation offset (bias), defaults to 0
    :type stim_bias: int, optional
    :param n_periods: Numer of periods of stimulus, defaults to None
    :type n_periods: [type], optional
    :param nostim_before: Time before stimulation, defaults to 0
    :type nostim_before: int, optional
    :param nostim_after: Time after stimulation, defaults to 0
    :type nostim_after: int, optional
    :raises ValueError: Raises error if unsupported stimulus type is chosen.
    :return: Stimulus timeseries
    :rtype: numpy.ndarray
    """
    """Constructs a sitmulus that can be applied as input to a model
https://tubcloud.tu-berlin.de/s/jm28fpm4kf4eETY
    TODO: rewrite

    stim:       Stimulus type: 'ac':oscillatory stimulus, 'dc': stimple step current, 
                'rect': step current in negative then positive direction with slowly
                decaying amplitude, used for bistability detection
    stim_amp:   Amplitude of stimulus (for AdEx: in mV/ms, multiply by conductance C to get current in pA)
    """
    def sinus_stim(f=1, amplitude=0.2, positive=0, phase=0, cycles=1, t_pause=0):
        x = np.linspace(np.pi, -np.pi, int(1000 / dt / f))
        sinus_function = np.hstack(((np.sin(x + phase) + positive), np.tile(0, t_pause)))
        sinus_function *= amplitude
        return np.tile(sinus_function, cycles)

    if stim == "ac":
        """Oscillatory stimulus
        """
        n_periods = n_periods or int(stim_freq)

        stimulus = np.hstack(
            ([stim_bias] * int(nostim_before / dt), np.tile(sinus_stim(stim_freq, stim_amp) + stim_bias, n_periods),)
        )
        stimulus = np.hstack((stimulus, [stim_bias] * int(nostim_after / dt)))
    elif stim == "dc":
        """Simple DC input and return to baseline
        """
        stimulus = np.hstack(([stim_bias] * int(nostim_before / dt), [stim_bias + stim_amp] * int(1 / dt)))
        stimulus = np.hstack((stimulus, [stim_bias] * int(nostim_after / dt)))
        stimulus[stimulus < 0] = 0
    elif stim == "rect":
        """Rectified step current with slow decay
        """
        # construct input
        stimulus = np.zeros(int(duration / dt))
        tot_len = int(duration / dt)
        stim_epoch = tot_len / 6

        stim_increase_counter = 0
        stim_decrease_counter = 0
        stim_step_increase = 5.0 / stim_epoch

        for i, m in enumerate(stimulus):
            if 0 * stim_epoch <= i < 0.5 * stim_epoch:
                stimulus[i] -= stim_amp
            elif 0.5 * stim_epoch <= i < 3.0 * stim_epoch:
                stimulus[i] = -np.exp(-stim_increase_counter) * stim_amp
                stim_increase_counter += stim_step_increase
            elif 3.0 * stim_epoch <= i < 3.5 * stim_epoch:
                stimulus[i] += stim_amp
            elif 3.5 * stim_epoch <= i < 5 * stim_epoch:
                stimulus[i] = np.exp(-stim_decrease_counter) * stim_amp
                stim_decrease_counter += stim_step_increase
    else:
        raise ValueError(f'Stimulus {stim} not found. Use "ac", "dc" or "rect".')

    # repeat stimulus until full length
    steps = int(duration / dt) 
    stimlength = int(len(stimulus))
    #print(stimlength)
    stimulus = np.tile(stimulus, int(steps / stimlength + 2))
    stimulus = stimulus[:steps]

    return stimulus

def construct_inphase_stimulus(   
    duration=6000,
    dt=0.1,
    stim_amp=0.2,
    stim_freq_in=1,
    stim_bias=0,
    n_periods_in=None,
    nostim_before=0,
    nostim_after=0,
    decay=False
):
    def sinus_stim(f=1, amplitude=0.2, positive=0, phase=0, cycles=1, t_pause=0):
        x = np.linspace(np.pi, -np.pi, int(1000 / dt / f))
        sinus_function = np.hstack(((np.sin(x + phase) + positive), np.tile(0, t_pause)))
        sinus_function *= amplitude
        return np.tile(sinus_function, cycles)
    
    def sinus_stim_decay(f=1, amplitude=0.2, positive=0, phase=0, cycles=1, t_pause=0):
        x = np.linspace(np.pi, -np.pi, int(1000 / dt / f))
        sinus_function = np.hstack(((np.sin(x + phase) + positive), np.tile(0, t_pause)))
        sinus_function *= amplitude
        stim = np.tile(sinus_function, cycles)

        decay_len = len(stim) / 6 # should decay to 1 / 6e
        stim_decrease_counter = 0
        stim_step_increase = 1/decay_len
        for i, m in enumerate(stim):
            if i < len(stim)/2:
                pass
            else:
                stim[i] *= np.exp(-stim_decrease_counter) 
                stim_decrease_counter += stim_step_increase
        return stim
    
    # create two stimuli and then hstack
    #### IN-PHASE STIMULUS
    n_periods_in = n_periods_in or int(stim_freq_in)
        
    if decay:
        oscillation_in_phase = sinus_stim_decay(stim_freq_in, stim_amp, cycles = n_periods_in)
    else:
        oscillation_in_phase = sinus_stim(stim_freq_in, stim_amp, cycles = n_periods_in)

    stimulus_in_phase = np.hstack(
        ([stim_bias] * int(nostim_before / dt), oscillation_in_phase + stim_bias)
    ) 
    stimulus = np.hstack((stimulus_in_phase, [stim_bias] * int(nostim_after / dt))) #could make variable nostim_between if necessary
    
    steps = int(duration / dt) 
    stimlength = int(len(stimulus))
    stimulus = np.tile(stimulus,int(steps / stimlength + 2))
    stimulus = stimulus[:steps]

    return stimulus
 
def construct_antiphase_stimulus(   
    duration=6000,
    dt=0.1,
    stim_amp=0.2,
    stim_freq_anti=2,
    stim_bias=0,
    n_periods_anti=None,
    nostim_before=0,
    nostim_after=0,
    decay=False
):
    def sinus_stim(f=1, amplitude=0.2, positive=0, phase=0, cycles=1, t_pause=0):
        x = np.linspace(np.pi, -np.pi, int(1000 / dt / f))
        sinus_function = np.hstack(((np.sin(x + phase) + positive), np.tile(0, t_pause)))
        sinus_function *= amplitude
        return np.tile(sinus_function, cycles)
    
    def sinus_stim_decay(f=1, amplitude=0.2, positive=0, phase=0, cycles=1, t_pause=0):
        x = np.linspace(np.pi, -np.pi, int(1000 / dt / f))
        sinus_function = np.hstack(((np.sin(x + phase) + positive), np.tile(0, t_pause)))
        sinus_function *= amplitude
        stim = np.tile(sinus_function, cycles)

        decay_len = len(stim) / 6 # should decay to 1 / 6e
        stim_decrease_counter = 0
        stim_step_increase = 1/decay_len
        for i, m in enumerate(stim):
            if i < len(stim)/2:
                pass
            else:
                stim[i] *= np.exp(-stim_decrease_counter) 
                stim_decrease_counter += stim_step_increase
        return stim
    
    ### ANTI-PHASE STIMULUS
    n_periods_anti = n_periods_anti or int(stim_freq_anti)
    
    if decay:
        oscillation_anti = sinus_stim_decay(stim_freq_anti, stim_amp, cycles = n_periods_anti)
    else:
        oscillation_anti = sinus_stim(stim_freq_anti, stim_amp, cycles = n_periods_anti)

    stimulus_anti_phase = np.hstack(
        ([stim_bias] * int(nostim_before / dt), oscillation_anti + stim_bias)
    )
    stimulus = np.hstack((stimulus_anti_phase, [stim_bias] * int(nostim_after / dt)))
    
    steps = int(duration / dt)
    stimlength = int(len(stimulus))
    stimulus = np.tile(stimulus, int(steps / stimlength + 2))
    stimulus = stimulus[:steps]
    stimulus_anti = -stimulus + 2*stim_bias
    
    stimuli = np.ndarray( (2, int(duration / dt)) )
    
    stimuli[0] = stimulus
    stimuli[1] = stimulus_anti
    return stimulus, stimulus_anti
 
def construct_inphase_antiphase_stimulus(   
    duration=6000,
    dt=0.1,
    stim_amp=0.2,
    stim_freq_in=1,
    stim_freq_anti=2,
    stim_bias=0,
    n_periods_in=None,
    n_periods_anti=None,
    nostim_before=0,
    nostim_after=0,
    nostim_between=0,
    decay=False
):
    def sinus_stim(f=1, amplitude=0.2, positive=0, phase=0, cycles=1, t_pause=0):
        x = np.linspace(np.pi, -np.pi, int(1000 / dt / f))
        sinus_function = np.hstack(((np.sin(x + phase) + positive), np.tile(0, t_pause)))
        sinus_function *= amplitude
        return np.tile(sinus_function, cycles)
    
    def sinus_stim_decay(f=1, amplitude=0.2, positive=0, phase=0, cycles=1, t_pause=0):
        x = np.linspace(np.pi, -np.pi, int(1000 / dt / f))
        sinus_function = np.hstack(((np.sin(x + phase) + positive), np.tile(0, t_pause)))
        sinus_function *= amplitude
        stim = np.tile(sinus_function, cycles)

        decay_len = len(stim) / 6 # should decay to 1 / 6e
        stim_decrease_counter = 0
        stim_step_increase = 1/decay_len
        for i, m in enumerate(stim):
            if i < len(stim)/2:
                pass
            else:
                stim[i] *= np.exp(-stim_decrease_counter) 
                stim_decrease_counter += stim_step_increase
        return stim
    
    # create two stimuli and then hstack
    #### IN-PHASE STIMULUS
    n_periods_in = n_periods_in or int(stim_freq_in)
        
    if decay:
        oscillation_in_phase = sinus_stim_decay(stim_freq_in, stim_amp, cycles = n_periods_in)
    else:
        oscillation_in_phase = sinus_stim(stim_freq_in, stim_amp, cycles = n_periods_in)

    stimulus_in_phase = np.hstack(
        ([stim_bias] * int(nostim_before / dt), oscillation_in_phase + stim_bias)
    ) 
    stimulus_in_phase = np.hstack((stimulus_in_phase, [stim_bias] * int(nostim_between / dt))) #could make variable nostim_between if necessary
    
    ### ANTI-PHASE STIMULUS
    n_periods_anti = n_periods_anti or int(stim_freq_anti)
    
    if decay:
        oscillation_anti = sinus_stim_decay(stim_freq_anti, stim_amp, cycles = n_periods_anti)
    else:
        oscillation_anti = sinus_stim(stim_freq_anti, stim_amp, cycles = n_periods_anti)

    stimulus_anti_phase = np.hstack(
        ([stim_bias] * int(nostim_before / dt), oscillation_anti + stim_bias)
    )
    stimulus_anti_phase = np.hstack((stimulus_anti_phase, [stim_bias] * int(nostim_after / dt)))
    

    stimulus = np.hstack((stimulus_in_phase, stimulus_anti_phase))
    stimulus_anti = np.hstack((stimulus_in_phase, -stimulus_anti_phase + 2*stim_bias))
    
    steps = int(duration / dt)
    stimlength = int(len(stimulus))
    stimulus = np.tile(stimulus, int(steps / stimlength + 2))
    stimulus = stimulus[:steps]
    stimulus_anti = np.tile(stimulus_anti, int(steps / stimlength + 2))
    stimulus_anti = stimulus_anti[:steps]
    
    stimuli = np.ndarray( (2, int(duration / dt)) )
    stimuli[0] = stimulus
    stimuli[1] = stimulus_anti
    return stimulus, stimulus_anti
 
        
def analyze_from_params(de,di):
    aln = ALNModel()
    aln.params['duration'] = 30 * 1000  # in ms
    # mean external exc/inh input (OU-process)
    aln.params['mue_ext_mean'] = 1.3
    aln.params['mui_ext_mean'] = 0.5 
    # no noise in OU-process
    aln.params['sigma_ou'] = 0.
    aln.params['de'] = de
    aln.params['di'] = di
    aln.run()
    signal = Signal.from_model_output(aln)
    plot_phase_shift_EI(signal,de,di)

def phase_shift(ts1, ts2, duration=1000, dt=0.1):
    ''' Gives phase shift of two timeseries. 
        Transforms timeseries into analytical phases and correlates them for phase shift.'''
    # find period
    start =  -int(duration/dt) 
    phase1 = np.angle(hilbert(ts1[start:])) # gives phase from hilbert transform
    phase2 = np.angle(hilbert(ts2[start:]))
    periods1 = np.where(np.diff(np.sign(phase1-np.mean(phase1)))==-2)[0] # downstrokes of phase (2 pi -> 0)
    periods2 = np.where(np.diff(np.sign(phase2-np.mean(phase2)))==-2)[0]
    #periods
    period1 = np.mean(np.diff(periods1))
    period2 = np.mean(np.diff(periods2))
    #ic(period1, period2)
    #assert np.isclose(period1, period2, rtol=0.05), "periods must be almost equal to compute meaningful phase shift"
    if not np.isclose(period1, period2, rtol=0.05):
        return np.nan, np.nan
    period = np.mean([period1, period2])
    period_s = period*dt / 1000 # period in sec
     
    # find phase shift by cross-correlating analytical phases
    xcorr = correlate(phase2, phase1)
    t = np.linspace(-len(phase1), len(phase1), 2*len(phase1)-1)
    #fig, ax = plt.subplots()
    #plt.plot(t, xcorr)
    dt_shift = t[xcorr.argmax()]
    #ic(dt_shift)
    # reduce dt_shift to interval [-T/2, T/2]
    while dt_shift < -period /2:
        dt_shift += period
    while dt_shift > period /2:
        dt_shift -= period
        
    phase_shift = 2 * dt_shift/period  # in [-1, 1]
        
    return phase_shift, dt_shift
    
def phase_shift_EI(rates_exc, rates_inh, state, dt=0.1):
    ''' Gives pPhase shift of inhibitory to excitatory activity in multiples of pi. 
        Transforms timeseries into analytical phases and correlates them for phase shift.'''
    if state != 0:
        return 0,0

    start =  -int(5 * 1000/dt) # start at -5 s (dt in signal is in seconds)
    end = -100
    exc_phase = np.angle(hilbert(rates_exc[0][start:end])) # gives phase from hilbert transform
    inh_phase = np.angle(hilbert(rates_inh[0][start:end]))
    starts_exc = np.where(np.diff(np.sign(exc_phase-np.mean(exc_phase)))==-2)[0] # downstrokes of phase (2 pi -> 0)
    starts_inh = np.where(np.diff(np.sign(inh_phase-np.mean(inh_phase)))==-2)[0]
    #periods
    period_exc = np.mean(np.diff(starts_exc))
    period_inh = np.mean(np.diff(starts_inh))
    period = np.mean([period_exc, period_inh])
    period_s = period*dt/1000
    
    # find phase shift by cross-correlating analytical phases
    start, end = - int(1000 / dt), -1 # correlate only last second
    exc_phase = exc_phase[start:end]
    inh_phase = inh_phase[start:end]
    xcorr = correlate(inh_phase,exc_phase)
    t = np.linspace(-len(exc_phase), len(exc_phase), 2*len(exc_phase)-1)
    t_shift = t[xcorr.argmax()]
    #print(t_shift," ", xcorr.argmax())
    
    #t_shift_s = t_shift*dt/1000
    phase_shift = t_shift/period * 2 # phase shift in multiples of pi
    # reduce phase shift to [-1,1] (multiples of pi)
    while phase_shift < -1:
        phase_shift = 2 + phase_shift
    while phase_shift > 1:
        phase_shift = -2 + phase_shift
        
    return phase_shift, 1./period_s


def classify_state(rates_exc, rates_inh, dt=0.1):
    ''' classifies signal into up, down and oscillatory state by looking at ratio mean/variance of exc. time series in stable regime
    '''

    start =  -int(5 * 1000/dt) # start at -5 s (dt in signal is in seconds)
    end = -1
    exc = rates_exc[0][start:end]
    inh = rates_inh[0][start:end]
    #ratio_exc = np.var(exc)/np.mean(exc)
    #ratio_inh = np.var(inh)/np.mean(inh)
    #if (ratio_exc > 0.01) & (ratio_inh > 0.01): 
    if (np.max(exc)-np.min(exc) > 0.1) | (np.max(inh)-np.min(inh) > 0.1):  # should be oscillating
        return 0
    elif np.max(exc) > 2: # with 2 Hz firing rate -> up state accoring to Caglar (in his new paper)
        return 1
    else: # else down state
        return -1 

def plot_phase_shift_EI(signal,de,di):
    ''' Gives phase shift of inhibitory to excitatory activity in multiples of pi '''
    dt = signal.dt
    start =  int(3/dt) # start at 3 s (dt in signal is in seconds)
    end = -100
    ##### PEAKS #######
    signal["rates_exc"].normalize()
    signal["rates_inh"].normalize()
    A = signal["rates_exc"].data.sel({"space":0})[start:end]
    B = signal["rates_inh"].data.sel({"space":0})[start:end]
    peaks_exc, _ = find_peaks(A,distance = 200)
    peaks_inh, _ = find_peaks(B,distance = 200)

    period_exc = np.mean(np.diff(peaks_exc))
    period_inh = np.mean(np.diff(peaks_inh))
    period = np.mean([period_exc,period_inh])
    period_s = period*dt
    
    # cut of first peak if one too many, and then calc shift if needed with period - shift
    if len(peaks_inh)==len(peaks_exc):
        pass
    elif len(peaks_inh)==len(peaks_exc)+1:
        peaks_inh = peaks_inh[1:]
    elif len(peaks_exc)==len(peaks_inh)+1:
        peaks_exc = peaks_exc[1:]    
    else:
        pass
        
    if np.mean(peaks_inh - peaks_exc) < period/2.:    
        time_shift = np.mean(peaks_inh - peaks_exc) 
    else: time_shift = period - np.mean(peaks_inh - peaks_exc) 

    phase_shift = time_shift/period
    
    ###### ANALYTICAL PHASE #####
    start = int(3/dt) # start at 3 s (dt in signal is in seconds)
    end = -100
    A2 = signal["rates_exc"].hilbert_transform(return_as="phase_wrapped",inplace=False).data.sel({"space":0})[start:end]
    B2 = signal["rates_inh"].hilbert_transform(return_as="phase_wrapped",inplace=False).data.sel({"space":0})[start:end]
    #zero_crossing_exc = np.where(np.diff(np.sign(A-np.mean(A))))
    #zero_crossing_inh = np.where(np.diff(np.sign(B-np.mean(B))))
    downstroke_exc = np.where(np.diff(np.sign(A2-np.mean(A2)))==-2)[0]
    downstroke_inh = np.where(np.diff(np.sign(B2-np.mean(B2)))==-2)[0]
    
    period_exc2 = np.mean(np.diff(downstroke_exc)) # in dt-stepsize
    period_inh2 = np.mean(np.diff(downstroke_inh))
    period2 = np.mean([period_exc2, period_inh2])
    period_s2 = period2*dt

    # if timeseries starts with inh downstroke, cut off first
    if len(downstroke_inh)>len(downstroke_exc):
        downstroke_inh = downstroke_inh[1:]

    time_shift2 = np.mean(downstroke_inh[0]-downstroke_exc[0])
    phase_shift2 = (time_shift2/period2) % 0.5 # cannnot be greater than pi, so 0.5*one period 

    ####### PLOT ######
    
    fig, (ax1,ax2) = plt.subplots(2,1,sharex=True,figsize=(15,10))

    ax1.plot(signal.data['time'][start:end],A)
    ax1.plot(signal.data['time'][start:end],B)
    ax1.scatter((peaks_exc+start)*dt, A[peaks_exc])
    ax1.scatter((peaks_inh+start)*dt, B[peaks_inh])
    ax1.set_title("Phase shift from PEAKS: {0:.2} with freq[Hz]: {1:.4}".format(phase_shift,1.0/period_s))
    ax2.plot(signal.data['time'][start:end],A2)
    ax2.plot(signal.data['time'][start:end],B2)
    ax2.scatter((downstroke_exc+start)*dt, A2[downstroke_exc])
    ax2.scatter((downstroke_inh+start)*dt, B2[downstroke_inh])
    ax2.set_title("Phase shift from an. PHASE: {0:.2} with freq[Hz]: {1:.4}".format(phase_shift2,1.0/period_s2))
    ax1.set_xlim(5, 10)
    ax2.set_xlim(5, 10)
    #ax1.set_ylim(13.75,13.85)
    plt.xlabel("t [s]")
    plt.ylabel("Firing Rate [Hz]")
    plt.suptitle("dE = {0:.3}, dI = {1:.3}".format(de, di),size=25)
    plt.show()


def plot_BoxSearch_timeseries(dfResults):
    ''' plots raster of timeseries for two parameter ranges dE,dI '''
    length_exc = len(dfResults['de'].drop_duplicates())
    length_inh = len(dfResults['di'].drop_duplicates())
    fig, axs = plt.subplots(length_exc,length_inh,constrained_layout=True,figsize=(17,10))
    for i,ax in enumerate(fig.axes):
        x = dfResults.de[i]
        y = dfResults.di[i]
        
        ax.set_title("de:{0:.3}, di:{1:.3}".format(x,y))
        ax.plot(dfResults['rates_exc'][i][0][-1000:-1])
        ax.plot(dfResults['rates_inh'][i][0][-1000:-1])
        #ax.plot(dfResults['rates_inh'][i][0][:-1],alpha=0.4)
        ax.axis('off')

# Old STUFF        
'''
def phase_shift_EI_old(rates_exc, rates_inh, state):
     Gives phase shift of inhibitory to excitatory activity in multiples of pi. 
        Transforms timeseries into analytical phases and takes mean difference of downstroke.
    if state != 0:
        return 0,0
    try: 
        dt = aln.params['dt']
    except:
        dt = 0.1
    start =  -int(5 * 1000/dt) # start at -5 s (dt in signal is in seconds)
    end = -100
    exc_phase = np.angle(hilbert(rates_exc[0][start:end])) # gives phase from hilbert transform
    inh_phase = np.angle(hilbert(rates_inh[0][start:end]))
    starts_exc = np.where(np.diff(np.sign(exc_phase-np.mean(exc_phase)))==-2)[0] # downstrokes of phase (2 pi -> 0)
    starts_inh = np.where(np.diff(np.sign(inh_phase-np.mean(inh_phase)))==-2)[0]
    #periods
    period_exc = np.mean(np.diff(starts_exc))
    period_inh = np.mean(np.diff(starts_inh))
    period = np.mean([period_exc, period_inh])
    period_s = period*dt/1000
    #phase shift 
    if len(starts_inh) == len(starts_exc) +1:
        starts_inh = starts_inh[1:]
    elif len(starts_inh) == len(starts_exc) -1:
        starts_exc = starts_exc[1:]
    elif len(starts_inh) > len(starts_exc) +1:
        starts_inh, starts_exc = np.nan, np.nan
    elif len(starts_inh) < len(starts_exc) -1:
        starts_inh, starts_exc = np.nan, np.nan
    
    t_shift = np.mean(starts_inh-starts_exc)
    t_shift_s = t_shift*dt/1000
    phase_shift = t_shift/period * 2 # phase shift in multiples of pi
    # reduce phase shift to [-1,1] (multiples of pi)
    if phase_shift < -1:
        phase_shift = 2 + phase_shift
    if phase_shift > 1:
        phase_shift = -2 + phase_shift
    return phase_shift, 1./period_s


def calc_phase_shift_EI_from_phase(signal):
     Gives phase shift of inhibitory to excitatory activity in multiples of pi. 
        Transforms timeseries into analytical phases and takes mean diff of downstroke.
        
    
    dt = signal.dt
    start =  -int(5/dt) # start at -5 s (dt in signal is in seconds)
    end = -100
    A2 = signal["rates_exc"].hilbert_transform(return_as="phase_wrapped",inplace=False).data.sel({"space":0})[start:end]
    B2 = signal["rates_inh"].hilbert_transform(return_as="phase_wrapped",inplace=False).data.sel({"space":0})[start:end]
    print("Analysing last {0:.3} ms".format(len(A2)/10))
    #zero_crossing_exc = np.where(np.diff(np.sign(A-np.mean(A))))
    #zero_crossing_inh = np.where(np.diff(np.sign(B-np.mean(B))))
    downstroke_exc = np.where(np.diff(np.sign(A2-np.mean(A2)))==-2)[0]
    downstroke_inh = np.where(np.diff(np.sign(B2-np.mean(B2)))==-2)[0] 

    period_exc2 = np.mean(np.diff(downstroke_exc))
    period_inh2 = np.mean(np.diff(downstroke_inh))
    period2 = np.mean([period_exc2, period_inh2])
    period_s2 = period2*dt

    # if timeseries starts with inh downstroke, cut off first
    if len(downstroke_inh)>len(downstroke_exc):
        downstroke_inh = downstroke_inh[1:]
        
    # if no phase then return nan
    try:
        time_shift2 = np.mean(downstroke_inh[0]-downstroke_exc[0])
        time_shift_s2 = time_shift2*dt
        phase_shift2 = (time_shift2/period2)*2 % 1 # phase shift in multiples of pi cannot be greater than 1
        #if phase_shift2 > 0.8:
        #    phase_shift2 = -1* (phase_shift2-0.5)
        #elif phase_shift2 < 0:
        #    phase_shift2 = 2 - phase_shift2 #if downstroke first 
        print("PHASE2: ",phase_shift2)
    except: 
        print("PHASE: ",np.nan)
        return np.nan, np.nan, np.nan
   
    return phase_shift2, time_shift_s2, 1.0/period_s2 # phase shift in multiples of pi, time shift in s, freq in Hz


def calc_phase_shift_EI_from_peaks(signal):
     Gives phase shift of inhibitory to excitatory activity in multiples of pi.
        Takes peak distances to calc phase shift
        
    dt = signal.dt
    start =  -int(5/dt) # start at 3 s (dt in signal is in seconds)
    end = -100
    signal.normalize()
    A = signal["rates_exc"].data.sel({"space":0})[start:end]
    B = signal["rates_inh"].data.sel({"space":0})[start:end]

    peaks_exc, _ = find_peaks(A,distance = 100) # height=5?
    peaks_inh, _ = find_peaks(B,distance = 100, prominence=1)

    period_exc = np.mean(np.diff(peaks_exc))
    period_inh = np.mean(np.diff(peaks_inh))
    period = np.mean([period_exc,period_inh])
    period_s = period*dt # period in seconds
    
    # cut of first peak if one too many, and then calc shift if needed with period - shift
    if len(peaks_inh)==len(peaks_exc):
        pass
    elif len(peaks_inh)==len(peaks_exc)+1:
        peaks_inh = peaks_inh[1:]
    elif len(peaks_exc)==len(peaks_inh)+1:
        peaks_exc = peaks_exc[1:]    
    else:
        return np.nan, np.nan, 1.0/period_s

    if abs(np.mean(peaks_inh - peaks_exc)) < period/2.:    
        time_shift = abs(np.mean(peaks_inh - peaks_exc) )
    else: time_shift = period - abs(np.mean(peaks_inh - peaks_exc))

    time_shift_s = time_shift*dt # time shift in seconds
    phase_shift = (time_shift/period)*2 % 1 # phase shift in multiples of pi cannot be greater than 1

    
    #xcorr = correlate(A,B)
    #t = np.linspace(-len(A), len(A), 2*len(A)-1)
    #time_shift = t[xcorr.argmax()]
    
    print("PEAKS: ",phase_shift)
    return phase_shift, time_shift_s, 1.0/period_s # phase shift in multiples of pi, time shift in seconds, freq in Hz



def calc_phase_shift_EI_old(signal):
     Gives phase shift of inhibitory to excitatory activity in multiples of pi 
     
    import numpy as np
    from scipy.signal import correlate, find_peaks
    
    phase_exc = signal["rates_exc"].hilbert_transform(return_as="phase_wrapped",inplace=False)
    phase_inh = signal["rates_inh"].hilbert_transform(return_as="phase_wrapped",inplace=False)
    dt = signal["dt"]
    length = len(signal["rates_exc"].data[0][0])
    length_ndt = length * dt
    # calc time difference by correlation with different time shifts for last 2/3 of time series
    analysis_len = np.round(length_ndt*2/3) 
    A = phase_exc.data[:,analysis_len:-30].sel({"space":0}) #analyze last 100 ms
    B = phase_inh.data[:,analysis_len:-30].sel({"space":0})
    xcorr = correlate(A,B)
    t = np.linspace(-len(A), len(A), 2*len(A)-1)
    time_shift = t[xcorr.argmax()]
    
    # calc period by average distance between maxima
    peaks_exc, _ = find_peaks(A,height=0)
    diff = np.diff(peaks_exc)
    period_exc = np.mean(diff[diff>100])
    peaks_inh, _ = find_peaks(B,height=0)
    diff = np.diff(peaks_inh)
    period_inh = np.mean(diff[diff>100])
    period = (period_exc+period_inh)/2.
    # calc phase difference 
    phase_shift = ( 2*time_shift/period ) 
    period = period*signal["dt"] # in ms
    #print("Phase shift = {0:.2f} *pi".format(phase_shift))
    return phase_shift, time_shift, 1.0/period

'''